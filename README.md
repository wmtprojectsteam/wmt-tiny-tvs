# WMT Tiny TVs

This Project covers the development of hardware and software to display still images on a 1 to 4 1.5" OLED Displays to simulate 1950s TVs in the TV / Radio Shops and the Hotels.

The code is written in Python and is based on the example code and driver provided by the Display manufacturer.
