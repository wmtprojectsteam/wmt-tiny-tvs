 # -*- coding:UTF-8 -*-
 ##
 # | file       :   display_tv_screens.py
 # | version    :   V0.6
 # | date       :   2021-05-01
 # | function   :   Display successive images on 1.5inch OLED screens
 #                  
 #
 # Permission is hereby granted, free of charge, to any person obtaining a copy
 # of this software and associated documnetation files (the "Software"), to deal
 # in the Software without restriction, including without limitation the rights
 # to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 # copies of the Software, and to permit persons to  whom the Software is
 # furished to do so, subject to the following conditions:
 #
 # The above copyright notice and this permission notice shall be included in
 # all copies or substantial portions of the Software.
 #
 # THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 # IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 # FITNESS OR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 # AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 # LIABILITY WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 # OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 # THE SOFTWARE.
 #
 
import DEV_Config
import OLED_Driver
import RPi.GPIO as GPIO
import time
import os
import random

from PIL import Image,ImageDraw

OLED = OLED_Driver.OLED()
DEV_Config.Driver_Delay_ms(20)
image = Image.new("L", (OLED.OLED_Dis_Column, OLED.OLED_Dis_Page), 0)# grayscale (luminance)
draw = ImageDraw.Draw(image)

# Define global variables
Number_of_Screens = 3                     # Set this to how many screens are installed (Max 3)

Kids_Dir = "/home/pi/TV_SW/Kids_Dir/"
Drama_Dir = "/home/pi/TV_SW/Drama_Dir/"
Documentaries_Dir = "/home/pi/TV_SW/Documentaries_Dir/"
Others_Dir = "/home/pi/TV_SW/Others_Dir/"

def index_dirs():
    global Kids_Dir, Drama_Dir, Documentaries_Dir, Others_Dir
    
    Kids_Files = []
    Drama_Files = []
    Documentaries_Files = []
    Others_Files = []
    
    # Index Kids Directory
    files = os.listdir(Kids_Dir)

    for f in files:
        Kids_Files.append(Kids_Dir + f)

    for i in Kids_Files:
        print(i)
    
    print("")

    # Index Drama Directory
    files = os.listdir(Drama_Dir)

    for f in files:
        Drama_Files.append(Drama_Dir + f)

    for i in Drama_Files:
        print(i)
    
    print("")

    # Index Documentaries Directory
    files = os.listdir(Documentaries_Dir)

    for f in files:
        Documentaries_Files.append(Documentaries_Dir + f)

    for i in Documentaries_Files:
        print(i)
    
    print("")

    # Index Others Directory
    files = os.listdir(Others_Dir)

    for f in files:
        Others_Files.append(Others_Dir + f)

    for i in Others_Files:
        print(i)
    
    print("")
    
    Image_List = Kids_Files + Drama_Files + Documentaries_Files + Others_Files
    
    return Image_List

def clear_screen():
    global image
    image = Image.open('All_Black.bmp')
    OLED.OLED_ShowImage(image,0,0)

def draw_line():
    global draw
    print ("***draw line")
    draw.line([(0,0),(127,0)], fill = "White",width = 1)
    draw.line([(127,0),(127,60)], fill = "White",width = 1)
    draw.line([(127,60),(0,60)], fill = "White",width = 1)
    draw.line([(0,60),(0,0)], fill = "White",width = 1)
    print ("***draw rectangle")
    draw.rectangle([(18,10),(110,20)],fill = "White")

def draw_text():
    global draw
    print ("***draw text")
    draw.text((33, 22), 'WaveShare ', fill = "White")
    draw.text((32, 36), 'Electronic ', fill = "White")
    draw.text((28, 48), '1.5inch OLED ', fill = "White")
        
def show_image(Image_List):
    global image

    OLED.OLED_ShowImage(image,0,0)
    DEV_Config.Driver_Delay_ms(2000)

    number = len(Image_List) - 1

    i = 0

    i = random.randrange(number)
    print(Image_List[i])

    image = Image.open(Image_List[i])

    OLED.OLED_ShowImage(image,0,0)

    time.sleep(10)

def write_to_screen(Pin, Image_List):
    global image

    GPIO.output(Pin, GPIO.LOW)
    clear_screen()
    draw_line()
    draw_text()
    show_image(Image_List)
    GPIO.output(Pin, GPIO.HIGH)
    
def main():
    Image_List = index_dirs()
    
    print ("********** Init OLED **********")
    OLED_ScanDir = OLED_Driver.SCAN_DIR_DFT  #SCAN_DIR_DFT = D2U_L2R
    OLED.OLED_Init(OLED_ScanDir)
    print ("")
    print ("")

# Set all Screen Selects HIGH
    GPIO.output(27, GPIO.HIGH)
    GPIO.output(22, GPIO.HIGH)
    GPIO.output(23, GPIO.HIGH)
    GPIO.output(18, GPIO.HIGH)

# Write to each screen in turn
    while 1:
        if Number_of_Screens == 1:
            print ("**** Write to Screen 1 *******")
            write_to_screen(DEV_Config.Screen_1_Select_Pin, Image_List)
            print ("")

        if Number_of_Screens == 2:
            print ("**** Write to Screen 1 *******")
            write_to_screen(DEV_Config.Screen_1_Select_Pin, Image_List)
            print ("")

            print ("**** Write to Screen 2 *******")
            write_to_screen(DEV_Config.Screen_2_Select_Pin, Image_List)
            print ("")

        if Number_of_Screens == 3:
            print ("**** Write to Screen 1 *******")
            write_to_screen(DEV_Config.Screen_1_Select_Pin, Image_List)
            print ("")

            print ("**** Write to Screen 2 *******")
            write_to_screen(DEV_Config.Screen_2_Select_Pin, Image_List)
            print ("")

            print ("**** Write to Screen 3 *******")
            write_to_screen(DEV_Config.Screen_3_Select_Pin, Image_List)
            print ("")

if __name__ == '__main__':
    main()

